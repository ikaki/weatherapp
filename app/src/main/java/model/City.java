package model;

import java.io.Serializable;

/**
 * Created by autumn on 4/9/18.
 */

public class City implements Serializable{

    int id;
    String name;
    String state;
    int temp;
    int maxTemp;
    int minTemp;
    int humidity;
    String iconURL;

    public City(String name, String state){
        this.name = name;
        this.state = state;
        this.temp = 0;
        this.maxTemp = 0;
        this.minTemp = 0;
        this.humidity = 0;
        this.id = 0;
        this.iconURL = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIconURL() {
        return iconURL;
    }

    public void setIconURL(String iconURL) {
        this.iconURL = iconURL;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getTemp() {
        return temp;
    }

    public void setTemp(int temp) {
        temp = (temp - 273)*9/5 + 32;
        this.temp = temp;
    }

    public int getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(int maxTemp) {
        maxTemp = (maxTemp - 273)*9/5 + 32;

        this.maxTemp = maxTemp;
    }

    public int getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(int minTemp) {
        minTemp = (minTemp - 273)*9/5 + 32;

        this.minTemp = minTemp;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }
}
