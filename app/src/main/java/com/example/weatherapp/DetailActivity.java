package com.example.weatherapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import model.City;


public class DetailActivity extends AppCompatActivity {
    private TextView nameView;
    private TextView tempView;
    private TextView lowHighView;
    private TextView precipView;
    private ImageView iconView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        City city = (City) getIntent().getSerializableExtra("city");

        nameView = findViewById(R.id.name);
        tempView = findViewById(R.id.temp);
        lowHighView = findViewById(R.id.low_high);
        precipView = findViewById(R.id.precipitation);
        iconView = findViewById(R.id.icon);

        nameView.setText(city.getName());

        String temp = Integer.toString(city.getTemp()) + (char) 0x00B0 + "F";
        tempView.setText(temp);

        String iconUrl = city.getIconURL();
        if (!iconUrl.equals("")){
            Picasso.with(this).load(iconUrl).into(iconView);
        }

        StringBuilder sb = new StringBuilder();
        sb.append(city.getMinTemp());
        sb.append((char) 0x00B0);
        sb.append("F / ");
        sb.append(city.getMaxTemp());
        sb.append((char) 0x00B0);
        sb.append("F");
        lowHighView.setText(sb.toString());

        precipView.setText(Integer.toString(city.getHumidity()) + "%");
    }
}
