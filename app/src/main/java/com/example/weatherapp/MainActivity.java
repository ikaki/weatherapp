package com.example.weatherapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import model.City;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<City>cities = new ArrayList<>();
    private FloatingActionButton addButton;
    private String apiKey = "da65fafb6cb9242168b7724fb5ab75e7";
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        context = this;

        City cityA = new City("San Francisco", "CA");
        City cityB = new City("New York", "NY");
        City cityC = new City("Salt Lake City", "UT");

        cities.add(cityA);
        cities.add(cityB);
        cities.add(cityC);

        adapter = new ListAdapter(cities, this);
        recyclerView.setAdapter(adapter);

        GetDataTask dataTask = new GetDataTask();
        City[] params= new City[]{cityA, cityB, cityC};
        dataTask.execute(params);

        addButton = findViewById(R.id.add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final EditText taskEditText = new EditText(context);
                AlertDialog dialog = new AlertDialog.Builder(context)
                        .setTitle("Add a new city")
                        .setMessage("What is the name of the city you want to add?")
                        .setView(taskEditText)
                        .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String name = String.valueOf(taskEditText.getText());
                                City newCity = new City(name, "");
                                cities.add(newCity);
                                adapter.notifyItemChanged(cities.size()-1);
                                GetDataTask dataTask = new GetDataTask();
                                dataTask.execute(newCity);
                            }
                        })
                        .setNegativeButton("Cancel", null)
                        .create();
                dialog.show();
            }
        });

    }
//  Task to update wather data for each city and reload recyclerAdapter
    public class GetDataTask extends AsyncTask<City, Void, City[]> {

        @Override
        protected City[] doInBackground(City...params) {
            for (City city : params) {
                StringBuilder sb = new StringBuilder("http://api.openweathermap.org/data/2.5/weather?q=");
                sb.append(city.getName());
                sb.append("&APPID=");
                sb.append(apiKey);

                try {
                    URL url = new URL(sb.toString());
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("GET");
                    urlConnection.connect();

                    if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        InputStream responseBody = urlConnection.getInputStream();
                        String responseJSON = readString(responseBody);
                        JSONObject jsonObj = new JSONObject(responseJSON);

                        city.setId(jsonObj.getInt("id"));
                        city.setTemp(jsonObj.getJSONObject("main").getInt("temp"));
                        city.setHumidity(jsonObj.getJSONObject("main").getInt("humidity"));
                        city.setMaxTemp(jsonObj.getJSONObject("main").getInt("temp_max"));
                        city.setMinTemp(jsonObj.getJSONObject("main").getInt("temp_min"));

                        String icon = jsonObj.getJSONArray("weather").getJSONObject(0).getString("icon") + ".png";
                        StringBuilder iconSb = new StringBuilder("http://openweathermap.org/img/w/");
                        iconSb.append(icon);
                        city.setIconURL(iconSb.toString());
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return params;
        }
        @Override
        protected void onPostExecute(City[] updateCities) {
            for (int i = 0; i < updateCities.length; i++){
                int position = cities.indexOf(updateCities[i]);
                cities.set(position, updateCities[i]);
                adapter.notifyItemChanged(position);
            }
        }
    }
    private String readString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStreamReader sr = new InputStreamReader(is);
        BufferedReader bReader = new BufferedReader(sr);
        String temp, response = "";

        while ((temp = bReader.readLine()) != null) {
            response += temp;
        }

        return response;
    }
}
