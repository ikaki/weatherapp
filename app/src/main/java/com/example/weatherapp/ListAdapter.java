package com.example.weatherapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.support.v7.widget.CardView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

import model.City;

/**
 * Created by autumn on 4/9/18.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
    private ArrayList<City> dataset;
    private Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CardView cardView;
        public ViewHolder(CardView v) {
            super(v);
            cardView = v;
        }
    }

    public ListAdapter(ArrayList<City> myDataset, Context context) {
        this.dataset = myDataset;
        this.context = context;
    }

    @Override
    public ListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        CardView v = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final City city = dataset.get(position);
        TextView nameView = holder.cardView.findViewById(R.id.text_view);
        TextView tempView = holder.cardView.findViewById(R.id.temp_view);
        ImageView iconView = holder.cardView.findViewById(R.id.image_view);
        LinearLayout l = holder.cardView.findViewById(R.id.linear_layout);

//      Bind values to each item
        nameView.setText(city.getName());

        String temp = Integer.toString(city.getTemp());
        tempView.setText(temp + (char) 0x00B0 + "F");

        String iconUrl = city.getIconURL();
        if (!iconUrl.equals("")){
            Picasso.with(context).load(iconUrl).into(iconView);
        }

//      Set click listener to open new activity on individual item click
        l.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("city", city);
                context.startActivity(intent);
            }
        });

//      Set long click listener to delete item on long individual item click
        l.setLongClickable(true);
        l.setOnLongClickListener(new View.OnLongClickListener(){

            @Override
            public boolean onLongClick(View view) {
                dataset.remove(position);
                notifyDataSetChanged();
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }
}
